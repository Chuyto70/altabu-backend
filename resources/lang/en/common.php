<?php

return [
    'error' => [
        'unauthorized' => 'Unauthorized',
        'unsupported_language' => 'Unsupported language for website',
        'translations_not_found' => 'Translations file not found',
    ],
    'message' => [
        'user_logout' => 'Successfully logged out',
    ],
];
