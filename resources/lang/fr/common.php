<?php

return [
    'error' => [
        'unauthorized' => 'Non autorisé',
        'unsupported_language' => 'Langue non prise en charge pour le site Web',
        'translations_not_found' => 'Fichier de traduction introuvable',
    ],
    'message' => [
        'user_logout' => 'Déconnexion réussie',
    ],
];
