@component('mail::message')
    <p>Hello {{ $user->name }}, click on the button below to reset the password</p>

@component('mail::button', ['url' => url('api/forgot-password/'.$user->remember_token)])
    Verify
@endcomponent

@endcomponent