# PDF Backend

This project is the backend service for the Dashboard projects. It provides RESTful API requests to be consumed by the Dashboard Frontend service.

<br/>

### Project Setup

To load the service on docker containers, copy `.env.example` and rename it to `.env` in the same directory. Configure the `.env` file accordingly, mainly by providing your system username, which can be found with the cli command `whoami`. Make sure to have docker installed and then run the following commands to build the required containers:

1. `docker network create global-network`
2. `docker-compose -f .docker/docker-compose.yaml up -d`

The containerisation setup works over a global network. This way the project containers can communicate with other project containers using the full name of the container name. This only works internally between containers, browsers do not understand this communication, so the container exposed URL should be used when browsing or using ajax requests.

Whether the setup is going to be in containers or on your local machine, run to following commands to install dependancies and prepare the setup. In case of using docker, run the below command like so: `docker exec -it [PHP container name] [command needed]`

1. `composer install`
2. `php artisan key:generate` ( only if APP_KEY is missing in your .env file )
3. `php artisan migrate:fresh --seed`
4. `php artisan generate:translations`

<br/>

### IDE Setup: VSCode

The project includes configurations for VSCode to get you started quickly with recommended extensions and settings.

To install recommanded extensions press Ctrl + Shift + P on your keyboard to open the command palette and search for `Extensions: Show Recommanded Extensions`. Once clicked a list of recommanded extensions will open in the side bar. At the top of the bar you can click the icon for `Install Workspace Recommanded Extensions` to install all extensions in one go.

<br/>

### Standards

For this project follow these standards:

-   [Naming Conventions](https://horizonweb.atlassian.net/wiki/spaces/Tech/pages/7635116/Naming+Conventions)
-   [Thunder Client Requests](https://horizonweb.atlassian.net/wiki/spaces/Tech/pages/9371649/Thunder+Client+Request)

To style the code for all PHP files using Laravel Pint run `./vendor/bin/pint` command.

<br/>

### Testing

In order to test our code we use PHP Unit for Feature and Unit tests and Thunder Client ( VSCode Extension ) for API testing.

In some cases Ngrok service must be running in the background for webhooks to reach local environment. Run `ngrok start http` before testing to start the service. The project will automatically handle the public URL for you.

Before running PHP Unit, make sure you have a `app-test` database created. In case docker is used, this database is already created. Make sure you have configured the env variables for the test database.

Run all PHP Unit tests with the following command:

`php artisan test`

Run one PHP Unit class tests like so:

`php artisan test tests/ExampleClassTest.php`

Run one specific PHP Unit function test:

`php artisan test --filter testExampleMethod`

Thunder Client must be installed as a VSCode extension in order to be able to test APIs. By default the VSCode should recommand you to install this extension as it's included in `.vscode/extensions.json`

To test the APIs, switch to Thunder Clients panel and run the request under test.

<br/>

### Commands

The following are custom or external package artisan commands:

-   `php artisan generate-translations`
    -   Generate translation JSON files for each locale and for each website.
