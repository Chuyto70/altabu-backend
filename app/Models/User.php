<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 8;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'language',
        'username',
        'token',
        'suspended',
        'likes',
        'phone',
        'city',
        'gender',
        'age',
        'height',
        'weight',
        'cup_size',
        'hair_color',
        'etnicity',
        'half',
        'hour',
        'night',
        'gallery',
        'avatar',
        'video',
        'facebook',
        'instagram',
        'twitter',
        'onlyfans',
        'pornhub',
        'token',
        'nationality',
        'galleryAlts',
        'videoAlt',
        'avatarAlt',
        'whatsapp',
        'description',
        '69_position',
        'blowjob_without_condom',
        'girlfrind_Experience',
        'erotic_massage',
        'tantric_massage',
        'foam_massage',
        'deepthroat',
        'dirty_talk',
        'facesitting',
        'gangbang',
        'for_couples',
        'BDSM',
        'bondage',
        'fetish',
        'foot_Fetish',
        'leather',
        'mistress'
        // ... add other missing fields
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'date_suspended' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->id = (string) Str::uuid();
        });

    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    /**
     * Get the messages for the user.
     */
    // public function messages(): HasMany
    // {
    //     return $this->hasMany(Message::class);
    // }
    public function desactivatedBy()
    {
        return $this->belongsToMany(Admins::class, 'admins', 'desactivated_users', 'user_id');
    }
}
