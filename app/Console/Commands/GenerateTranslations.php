<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class GenerateTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:translations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates json translation files from php files in lang folder';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        foreach (config("dashboard.locales") as $locale) {

            $jsonData = [];

            $directory = resource_path("lang/$locale");
            $files = array_diff(scandir($directory), ['.', '..']);

            foreach ($files as $file) {
                $filePathInfo = pathinfo($file);

                if ($filePathInfo['extension'] == 'php') {
                    $filename = $filePathInfo['filename'];
                    try {
                        $jsonData = array_merge($jsonData, [$filename => Lang::get("dashboard/$filename", [], $locale)]);
                    } catch (\Exception $e) {
                        Log::info($e);
                        exit;
                    }
                }
            }

            $directory = resource_path("lang/$locale");
            $files = array_diff(scandir($directory), ['.', '..']);

            $ignoreLocaleTranslations = [
                'mail',
            ];

            foreach ($files as $file) {
                $filePathInfo = pathinfo($file);

                if (array_key_exists('extension', $filePathInfo) && $filePathInfo['extension'] == 'php' && ! in_array($filePathInfo['filename'], $ignoreLocaleTranslations)) {
                    $filename = $filePathInfo['filename'];
                    try {
                        $jsonData = array_merge($jsonData, [$filename => Lang::get("$filename", [], $locale)]);
                    } catch (\Exception $e) {
                        Log::info($e);
                        exit;
                    }
                }
            }

            $translationsFilePath = resource_path("lang/$locale/translations.json");
            if (File::exists($translationsFilePath)) {
                unlink($translationsFilePath);
            }

            File::put($translationsFilePath, json_encode($jsonData));
        }
    }
}
