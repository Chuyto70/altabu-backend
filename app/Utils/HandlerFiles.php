<?php

namespace App\Utils;

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;

class HandlerFiles
{
    static function handleFiles(Request $request, User $user)
    {
        HandlerFiles::handleVideo($request, $user);
        HandlerFiles::handleAvatar($request, $user);
        HandlerFiles::handleGallery($request, $user);
    }

    static function handleVideo(Request $request, User $user)
    {
        $video = $request->file('video');

        if ($video) {
            HandlerFiles::deleteFileIfExists($user->video, 'videos');
            $videoUrl = HandlerFiles::storeVideo($video);
            $user->video = $videoUrl;
            $user->save();
        }
    }

    static function handleAvatar(Request $request, User $user)
    {
        $avatar = $request->file('avatar');

        if ($avatar) {
            HandlerFiles::deleteFileIfExists($user->avatar, 'avatars');
            $avatarUrl = HandlerFiles::storeAvatar($avatar);
            $user->avatar = $avatarUrl;
            $user->save();
        }
    }

    static function handleGallery(Request $request, User $user)
    {
        $gallery = $request->file('gallery');

        if ($gallery) {
            $galleryUrls = HandlerFiles::storeGalleryImages($gallery, $user);
            $galleryUrls_json = json_encode($galleryUrls);
            $user->gallery = $galleryUrls_json;
            $user->save();
        }
    }

    static function deleteFileIfExists($file, $folder)
    {
        if (isset($file) && $file !== '') {
            FilesUtils::deleteFile($file, $folder);
        }
    }

    static function storeVideo(UploadedFile $file)
    {
        return FilesUtils::storeVideo($file);
    }

    static function storeAvatar(UploadedFile $file)
    {
        return FilesUtils::storeAvatar($file);
    }

    static function storeGalleryImages(array $files, User $user)
    {
        return FilesUtils::storeGalleryImages($files, $user);
    }
   
    
}