<?php

namespace App\Utils;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Facades\Storage;


class FilesUtils
{
   public static function storeAvatar(UploadedFile $avatarImage)
    {
        $uniqueAvatarName = uniqid() . '.' . $avatarImage->getClientOriginalExtension();

        Storage::disk('avatars')->put($uniqueAvatarName, file_get_contents($avatarImage));
        return Storage::disk('avatars')->url($uniqueAvatarName);
    }

    public static function deleteFile($path_image, $disk_name)
    {
        $file_name = basename($path_image);
        if (Storage::disk($disk_name)->exists($file_name)) {
            Storage::disk($disk_name)->delete($file_name);
            return true;
        }else {
            return false;
        }
    }

    // public static function deleteFiles($path_image_arr, $disk_name)
    // {
    //     $galleryUrls = json_decode($path_image_arr);
    //     return $galleryUrls;
    // }

    public static function storeGalleryImages(array $galleryImages, $user = null)
    {
        if(!$user){
            $galleryUrls = [];
            foreach ($galleryImages as $image) {
                $uniqueFilename = uniqid() . '.' . $image->getClientOriginalExtension();
    
                Storage::disk('gallery')->put($uniqueFilename, file_get_contents($image));
                $galleryUrls[] = Storage::disk('gallery')->url($uniqueFilename);
            }
            return $galleryUrls;
        }else {
            
            $limit = 5;
            $galleryUrls = json_decode($user->gallery);
            $current_images_count = count($galleryUrls);
            
            $new_images_count = count($galleryImages);
            $total_images = $current_images_count + $new_images_count;

            if($total_images < $limit){
               
                foreach ($galleryImages as $image_file) {
                     $uniqueFilename = uniqid() . '.' . $image_file->getClientOriginalExtension();
                    
                    Storage::disk('gallery')->put($uniqueFilename, file_get_contents($image_file));
                    $galleryUrls[] = Storage::disk('gallery')->url($uniqueFilename);
                }
                return $galleryUrls;
            }


        }
    }

    public static function storeVideo(UploadedFile $videoFile)
    {
        $uniqueVideoName = uniqid() . '.' . $videoFile->getClientOriginalExtension();

        Storage::disk('videos')->put($uniqueVideoName, file_get_contents($videoFile));
        return Storage::disk('videos')->url($uniqueVideoName);
    }
}
