<?php

namespace App\Utils;

use App\Models\User;

class TransformData
{
    public static function transformOneUser(User $user)
    {
        $galleryWithAlts = [];
        $galleryUrls = json_decode($user->gallery);
        $galleryAlts = json_decode($user->galleryAlts);
        // dd($galleryAlts);
        for ($i = 0; $i < count($galleryUrls); $i++) {
            $galleryWithAlts[] = [
                'img' => $galleryUrls[$i],
                'alt' => $galleryAlts[$i],
            ];
        }

        return [
            'id' => $user->id,
            'username' => $user->username,
            'email' => $user->email,
            'phone' => $user->phone,
            'whatsapp' => $user->whatsapp,
            'city' => $user->city,
            'gender' => $user->gender,
            'age' => $user->age,
            'height' => $user->height,
            'weight' => $user->weight,
            'cup_size' => $user->cup_size,
            'hair_color' => $user->hair_color,
            'etnicity' => $user->etnicity,
            'nationality' => $user->nationality,
            'prices' => [
                'half' => $user->half,
                'hour' => $user->hour,
                'night' => $user->night,
            ],
            'services' => [
                '69_position' => $user['69_position'],
                'blowjob_without_condom' => $user->blowjob_without_condom,
                'girlfrind_Experience' => $user->girlfrind_Experience,
                'erotic_massage' => $user->erotic_massage,
                'tantric_massage' => $user->tantric_massage,
                'foam_massage' => $user->foam_massage,
                'deepthroat' => $user->deepthroat,
                'dirty_talk' => $user->dirty_talk,
                'facesitting' => $user->facesitting,
                'gangbang' => $user->gangbang,
                'for_couples' => $user->for_couples,
                'BDSM' => $user->BDSM,
                'bondage' => $user->bondage,
                'fetish' => $user->fetish,
                'foot_Fetish' => $user->foot_Fetish,
                'leather' => $user->leather,
                'mistress' => $user->mistress,
            ],
            'facebook' => $user->facebook,
            'instagram' => $user->instagram,
            'twitter' => $user->twitter,
            'onlyfans' => $user->onlyfans,
            'pornhub' => $user->pornhub,
            'avatar' => [
                'img' => $user->avatar,
                'alt' => $user->avatarAlt,
            ],
            'description' => $user->description,
            'gallery' => $galleryWithAlts,
            'video' => [
                'url' => $user->video,
                'alt' => $user->videoAlt,
            ],

        ];
    }
}
