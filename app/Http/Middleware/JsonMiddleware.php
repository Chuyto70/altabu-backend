<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;

class JsonMiddleware
{
    /**
     * The Response Factory our app uses
     *
     * @var ResponseFactory
     */
    protected $factory;

    /**
     * JsonMiddleware constructor.
     */
    public function __construct(ResponseFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->get('route') === 'download-file') {
            return $next($request);
        }

        if (env('APP_DEBUG')) {
            //Allows to view HTML content, helpful when testing mail designs
            if (in_array('text/html', (explode(',', $request->headers->get('Accept'))))) {
                $response = $next($request);

                return $response;
            }
        }

        // First, set the header so any other middleware knows we're
        // dealing with a should-be JSON response.
        $request->headers->set('Accept', 'application/json');

        // Get the response
        $response = $next($request);

        // If the response is not strictly a JsonResponse, we make it
        if (! $response instanceof JsonResponse && ! is_array($response->original)) {
            $response = response()->json([
                'result' => true,
                'type' => 'success',
                'data' => $response->content(),
            ], $response->status());
        } else {
            //If result was already added don't add it such as an error response
            if (isset(json_decode($response->content())->result)) {
                $response = response()->json(
                    json_decode($response->content(), true), $response->status()
                );
            } else {
                $response = response()->json([
                    'result' => true,
                    'type' => 'success',
                    'data' => json_decode($response->content(), true),
                ], $response->status());
            }

        }

        return $response;
    }
}
