<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RestrictOriginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $host = request()->headers->get('Host');
        $allowedDomains = explode(',', config('app.allowed_domains'));

        if (in_array($host, $allowedDomains)) {
            abort(403, 'Unauthorized origin');
        }

        // Pasar la petición al siguiente middleware
        return $next($request);

    }
}
