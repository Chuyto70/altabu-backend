<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;

class SetLocaleMiddleware
{
    /**
     * Handle an incoming request.
     */
    public function handle($request, Closure $next): Response
    {
        $locale = $request->header('Accept-Language') ?? 'en';
        
        if ($locale) {
            app()->setLocale($locale);
        }

        return $next($request);
    }
}
