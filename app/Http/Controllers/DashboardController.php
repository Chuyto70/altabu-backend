<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Sanctum\PersonalAccessToken;

class DashboardController extends Controller
{
    public function getConfig(Request $request)
    {
        $locales = config('dashboard.locales');

        
        $token = PersonalAccessToken::findToken($request->bearerToken());
        
        $user = $token ? $token->tokenable->load('roles') : null;
        
        
        if ($user) {
            $user->setRelation(
                'roles',
                $user->roles->map(function ($role) {
                    return [
                        'id' => $role->id,
                        'name' => $role->name,
                    ];
                })
            );
        }

        $data = [
            'locales' => $locales,
            'user' => $user,
        ];

        return $data;
    }
}
