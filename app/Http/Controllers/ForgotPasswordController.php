<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPasswordMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    public function sendResetLinkEmail(Request $request)
    {
        $request->validate(['email' => 'required|email|exists:users,email']);
        try {

            $user = User::where('email', $request->email)->first();

            $user->remember_token = Str::random(40);

            $user->save();

            Mail::to($user->email)->send(new ForgotPasswordMail($user));

            return response()->json(['status' => true, 'message' => 'Email sent!']);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'message' => 'Somethin went wrong', 'error' => $th], 404);
        }

    }

    public function redirectToClient(Request $request, $hash)
    {
        return redirect(config('app.frontend_url').'?token='.$hash);
    }

    public function changePassword(Request $request)
    {

        $request->validate([
            'password' => 'required|string|confirmed|min:6|max:16',
            'remember_token' => 'required|exists:users,remember_token',
        ]);
        try {
            $user = User::where('remember_token', $request->remember_token)->first();

            $user->password = Hash::make($request->password);
            $user->remember_token = null;

            $user->save();

            return response()->json(['status' => true, 'message' => 'Password changed successfully'], 200);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'message' => 'Somethin went wrong', 'error' => $th], 404);
        }

    }
}
