<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Utils\TransformData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $data = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ])->validated();

        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $request->user()->tokens()->delete();
            $token = $request->user()->createToken('API_TOKEN')->plainTextToken;
            $user = User::where('email', $data['email'])->first();
            $user->token = $token;

            $user->save();

            return [
                'token' => $token,
                'user' => TransformData::transformOneUser($user),
            ];
        } else {
            throw ValidationException::withMessages(['User does not match']);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     */
    public function logout(Request $request)
    {
        try {
            $user_id = $request->user()->id;
            User::where('id', $user_id)->update(['token' => null]) ;
            $request->user()->tokens()->delete();
        } catch (\Exception $e) {
            throw ValidationException::withMessages([$e->getMessage()]);
        }

        return ['message' => 'Logout successfully'];
    }

    /**
     * Refresh a token.
     */
    public function refresh(Request $request)
    {
        try {
            // Revoke the current token before creating a new one
            $request->user()->tokens->each(function ($token) {
                $token->delete();
            });

            // Create a new token
            $newToken = $request->user()->createToken(env('APP_NAME'))->plainTextToken;
        } catch (\Exception $e) {
            throw ValidationException::withMessages([$e->getMessage()]);
        }

        return ['token' => $newToken];
    }

    /**
     * Get user data.
     */
    public function getUserData(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Check if the user is logged in.
     */
    public function checkToken(Request $request)
    {
        $isLoggedIn = $request->user() ? true : false;

        return response()->json(['is_logged_in' => $isLoggedIn]);
    }
}
