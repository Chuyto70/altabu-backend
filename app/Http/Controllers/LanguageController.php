<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LanguageController extends Controller
{
    public function getTranslations(Request $request)
    {
        Validator::make($request->json()->all(), [
            'language' => 'required|string',
        ])->validate();

        $locale = $request->get('language');

        if (! in_array($locale, config("dashboard.locales"))) {
            throw ValidationException::withMessages(['common.error.unsupported_language']);
        }

        $translationsFilePath = resource_path("lang/$locale/translations.json");

        if (! File::exists($translationsFilePath)) {
            throw ValidationException::withMessages(['common.error.translations_not_found']);
        }

        $translations = File::get($translationsFilePath);

        return json_decode($translations, true);
    }
}
