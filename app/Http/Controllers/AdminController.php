<?php

namespace App\Http\Controllers;

use App\Models\Admins;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'username' => 'required|string|max:255',
            'email' => 'required|email|unique:admins',
            'password' => 'required|confirmed|min:6',
        ]);

        $admin = Admins::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $admin->token =  $admin->createToken('API_TOKEN')->plainTextToken;
        $admin->save();
       
        return response()->json(['status' => true, 'token' => $admin->token]);
    }

    public function login(Request $request)
    {
        // Validar los datos del formulario
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::guard('admin')->attempt($credentials)) {
            Auth::guard('admin')->user()->tokens()->delete();
            $token = Auth::guard('admin')->user()->createToken('API_TOKEN')->plainTextToken;
            $user = Admins::where('email', $credentials['email'])->first();
            $user->token = $token;

            $user->save();
            return response()->json([
                'token' => $token,
                'user' => $user,], 200);
        } else {
            return response()->json(['status'=>false, 'email' => 'Credentials do not match'], 403);
        }
    }

    public function logout(Request $request)
    {
        try {
            $user_id = Auth::user()->id;
            Admins::where('id', $user_id)->update(['token' => null]) ;
            $request->user()->tokens()->delete();
        } catch (\Exception $e) {
            throw ValidationException::withMessages([$e->getMessage()]);
        }

        return ['message' => 'Logout successfully'];
    }
    public function suspendUser(Request $request,  $id)
    { 
        try {
            $admin = $request->user();
            $listDesactivatedUsers =  json_decode($admin->desactivated_users) ?? [];
            $user = User::findOrFail($id);

            if ($user->suspended) {
                return response()->json(['status' => false, 'message' => 'User is already suspended']);
            }
            $user->suspended = true;
            $user->date_suspended = now();
            $user->save();
            if(!in_array($id, $listDesactivatedUsers)){
                $listDesactivatedUsers[] = $id;
                $admin->desactivated_users = json_encode($listDesactivatedUsers);
                $admin->save();
            }

            return response()->json(['status' => true, 'message' => 'User has been suspended']);
        } catch (ModelNotFoundException $th) {
            return response()->json(['status' => false, 'message' => 'Error searching this user']);
        }
    }

    public function removeSuspendUser(Request $request, $id)
    { 
        try {
            $admin = $request->user();
            $list_desactivated_users =  json_decode($admin->desactivated_users, ) ?? [];
            $user = User::findOrFail($id);

            if (!$user->suspended) {
                return response()->json(['status' => false, 'message' => 'User is not suspended'], 400);
            }
            $user->suspended = false;
            $user->date_suspended = null;
            $user->save();

             if(in_array($id, $list_desactivated_users)){
                unset($list_desactivated_users[array_search($id, $list_desactivated_users)]);
                $admin->desactivated_users = json_encode($list_desactivated_users);
                $admin->save();
            }

            return response()->json(['status' => true, 'message' => 'User is now active']);
        } catch (ModelNotFoundException $th) {
            return response()->json(['status' => false, 'message' => 'Error searching this user']);
        }
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->tokens()->delete();
            $user->delete();

            return response()->json(['status' => true, 'message' => 'User deleted'], 200);
        } catch (ModelNotFoundException  $th) {
            return response()->json(['status' => false, 'message' => 'Error searching this user'], 400);
        }
        
    }
}
