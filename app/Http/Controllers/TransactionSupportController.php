<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Rules\TransactionExists;
use App\Models\PswMysql\Transaction;
use App\Repositories\PaymentService\PaymentServiceRepository;

class TransactionSupportController extends Controller
{
    public function search(Request $request)
    {
        // Validate the incoming request data
        $request->validate([
            'id' => 'nullable|integer',
            'email' => 'nullable|email',
            'name' => 'nullable|string',
            'surname' => 'nullable|string',
            'last_four_digits' => 'nullable|integer|digits:4',
            'websites' => 'nullable|array',
            'websites.*' => 'integer',
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date',
        ]);
        
        // Retrieve input data from the validated request
        $id = $request->input('id');
        $email = $request->input('email');
        $name = $request->input('name');
        $surname = $request->input('surname');
        $lastFourDigits = $request->input('last_four_digits');
        $websites = $request->input('websites');
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $perPage = $request->input('per_page', 10);
        $page = $request->input('page', 1);

        // Initialize the query with eager-loaded relationships
        $query = Transaction::query()->with([
            'customer', 
            'creditCard', 
            'currency',
            'country',
            'website.service'
        ]);

        // Add conditions based on input criteria
        if ($id) {
            $query->where('id', $id);
        }
        if ($startDate && $endDate) {
            $query->whereBetween('created_at', [Carbon::parse($startDate), Carbon::parse($endDate)]);
        }
        
        if ($email) {
            $query->whereHas('customer', function($query) use ($email) {
                $query->where('email', 'LIKE', "%$email%");
            });
        }

        // Query the related credit card based on name, surname, or last four digits
        if ($name || $surname || $lastFourDigits) {
            $query->whereHas('creditCard', function($query) use ($lastFourDigits, $name, $surname) {
                if ($name) {
                    $query->where('cardholder_name', 'LIKE', "%$name%");
                }
                if ($surname) {
                    $query->where('cardholder_name', 'LIKE', "%$surname%");
                }
                if ($lastFourDigits) {
                    $query->where('last_four_digits', 'LIKE', '%' . $lastFourDigits);
                }
            });
        }

        // Query the related and website
        if ($websites) {
            $query->whereIn('website_id', $websites);
        }

        // Execute the query and paginate the results
        $response = $query->orderBy('created_at', 'desc')->paginate($perPage, ['*'], 'page', $page);

        // Modify the structure of the response
        $transactions = $response->map(function ($transaction) {
            // Split the cardholder_name into Name and Surname
            $nameParts = explode(' ', optional($transaction->creditCard)->cardholder_name, 2);

            return [
                'id' => $transaction->id,
                'email' => $transaction->customer->email,
                'name' => $nameParts[0] ?? null,
                'surname' => isset($nameParts[1]) ? $nameParts[1] : null,
                'date_of_transaction' => $transaction->created_at,
                'currency' => $transaction->currency->code,
                'country' => $transaction->country->code_alpha_2,
                'website' => $transaction->website->name,
            ];
        });

        $responseArray = $response->toArray();
        $responseArray['data'] = $transactions;

        // Return the transaction details in JSON format
        return response()->json($responseArray);
    }

    public function refund(Request $request, PaymentServiceRepository $paymentServiceRepository) {
        $request->validate([
            'transaction_id' => ['required', 'integer', new TransactionExists('psw_mysql')],
        ]);

        $data = $request->all();

        $transaction = Transaction::find($request->input('transaction_id'));

        $websiteName = $transaction->website->name;

        $data['website_name'] = $websiteName;

        $response = $paymentServiceRepository->refund($data);

        $responseData = json_decode($response, true);

        return $responseData;
    }

}
