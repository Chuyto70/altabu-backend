<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Models\User;
use App\Traits\ValidatesUserRequest;
use App\Utils\FilesUtils;
use App\Utils\HandlerFiles;
use App\Utils\TransformData;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;

class UserController extends Controller
{
    use ValidatesUserRequest;

    /**
     * Display a listing of all users.
     */
    public function index()
    {
        try {
            $users = User::simplePaginate();
            $users->getCollection()->transform(function ($user) {
                return TransformData::transformOneUser($user);
            });

            return $users;
        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function filter(Request $request)
    {
        try {
            $params = $request->query();
            unset($params['page']);

            $users = User::query()
                ->where(function ($query) use ($params) {
                    foreach ($params as $key => $value) {
                        if (! Schema::hasColumn('users', $key)) {
                            throw new InvalidArgumentException('The param '.$key.' is invalid');
                        } elseif (strstr($value, '-')) {
                            $arr_values = explode('-', $value);
                            $upperBound = (isset($arr_values[1])) ? intval($arr_values[1]) : null;
                            $lowerBound = intval($arr_values[0]);

                            $query->whereBetween($key, [$lowerBound, $upperBound]);
                        } elseif ($key === 'video') {
                            $query->whereNotNull($key);
                        } elseif ($key === 'city') {
                            $query->where('city', 'like', $value.'%');
                        } elseif ($key === 'username') {
                            $query->where('username', 'like', $value.'%');
                        } else {
                            $query->where($key, $value);
                        }
                    }
                })
                ->simplePaginate();
            $users->getCollection()->transform(function ($user) {
                return TransformData::transformOneUser($user);
            });

            return response()->json($users->withQueryString($params), 200);
        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function getUser($username)
    {
        try {
            $user = User::where('username', $username)->first();
            if ($user) {
                $userData = TransformData::transformOneUser($user);

                return response()->json([
                    'status' => true,
                    'user' => $userData,
                ], 200);
            } else {
                return response()->json(['status' => false, 'message' => 'User not found'], 404);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $this->validateStoreUserRequest($request);
        try {
            $avatarImage = $request->file('avatar');
            $avatarUrl = FilesUtils::storeAvatar($avatarImage);

            $galleryImages = $request->file('gallery');
            $galleryUrls = FilesUtils::storeGalleryImages($galleryImages);

            $videoFile = $request->file('video');
            $videoUrl = FilesUtils::storeVideo($videoFile);

            $user = User::create([
                'username' => $request->input('username'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'whatsapp' => $request->input('whatsapp'),
                'city' => $request->input('city'),
                'gender' => $request->input('gender'),
                'age' => $request->input('age'),
                'height' => $request->input('height'),
                'weight' => $request->input('weight'),
                'half' => $request->input('half'),
                'hour' => $request->input('hour'),
                'night' => $request->input('night'),
                'cup_size' => $request->input('cup_size'),
                'hair_color' => $request->input('hair_color'),
                'etnicity' => $request->input('etnicity'),
                'nationality' => $request->input('nationality'),
                'avatar' => $avatarUrl,
                'video' => $videoUrl,
                'videoAlt' => $request->input('videoAlt'),
                'avatarAlt' => $request->input('avatarAlt'),
                'gallery' => json_encode($galleryUrls),
                'galleryAlts' => json_encode($request->input('galleryAlts')),
                'password' => Hash::make($request->input('password')),
                'facebook' => $request->input('facebook'),
                'instagram' => $request->input('instagram'),
                'twitter' => $request->input('twitter'),
                'onlyfans' => $request->input('onlyfans'),
                'pornhub' => $request->input('pornhub'),
                'description' => $request->input('description'),
                'language' => $request->input('language'),
                '69_position' => $request->input('69_position'),
                'blowjob_without_condom' => $request->input('blowjob_without_condom'),
                'girlfrind_Experience' => $request->input('girlfrind_Experience'),
                'erotic_massage' => $request->input('erotic_massage'),
                'tantric_massage' => $request->input('tantric_massage'),
                'foam_massage' => $request->input('foam_massage'),
                'deepthroat' => $request->input('deepthroat'),
                'dirty_talk' => $request->input('dirty_talk'),
                'facesitting' => $request->input('facesitting'),
                'gangbang' => $request->input('gangbang'),
                'for_couples' => $request->input('for_couples'),
                'BDSM' => $request->input('BDSM'),
                'bondage' => $request->input('bondage'),
                'fetish' => $request->input('fetish'),
                'foot_Fetish' => $request->input('foot_Fetish'),
                'leather' => $request->input('leather'),
                'mistress' => $request->input('mistress'),
            ]);

            $user->token = $user->createToken('API_TOKEN')->plainTextToken;
            $user->save();
            $userData = TransformData::transformOneUser($user);
            SendEmail::dispatch($user);

            return [
                'status' => true,
                'token' => $user->token,
                'user' => $userData,

            ];
        } catch (\Exception $e) {
            throw ValidationException::withMessages(['status' => false, 'error' => $e->getMessage()]);
        }

    }

    /**
     * Update the specified user in BBDD.
     */
    public function update(Request $request, $id)
    {
        $this->validateUpdateUserRequest($request, $id);
        $user = Auth::user();

        if (! $user || $user->id != $id) {
            return response()->json(['status' => false, 'error' => 'Unauthorized'], 401);
        }

        $data = $request->except(['gallery', 'avatar', 'video']);
        $user = User::find($id);

        try {
            HandlerFiles::handleFiles($request, $user);
            $user->update($data);

            return response()->json(['status' => true, 'message' => 'User has been updated'], 200);
        } catch (\Exception $e) {
            throw ValidationException::withMessages(['status' => false, 'error' => $e->getMessage()]);
        }

    }

    public function deleteAvatar(Request $request, $userId, $filename)
    {
        $user = Auth::user();

        if (! $user || $user->id != $userId) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        try {
            $user = User::findOrFail($userId);
            if (Storage::disk('avatars')->exists($filename)) {
                FilesUtils::deleteFile($filename, 'avatars');
                $user->avatar = null;
                $user->save();
            }

            return response()->json(['status' => true, 'message' => 'Avatar deleted successfully'], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['status' => false, 'error' => 'User not found'], 404);
        }
    }

    public function deleteVideo(Request $request, $userId, $filename)
    {
        $user = Auth::user();

        if (! $user || $user->id != $userId) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        try {
            $user = User::findOrFail($userId);

            if (Storage::disk('videos')->exists($filename)) {
                FilesUtils::deleteFile($filename, 'videos');
                $user->video = null;
                $user->save();
            }

            return response()->json(['status' => true, 'message' => 'Video deleted successfully'], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['status' => false, 'error' => 'User not found'], 404);
        }
    }

    public function deleteGallery(Request $request, $userId, string $filename)
    {
        $user = Auth::user();

        if (! $user || $user->id != $userId) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        try {
            $user = User::findOrFail($userId);
            $gallery_urls_bbdd = json_decode($user->gallery);
            $filename_url = Storage::disk('gallery')->url($filename);

            if (Storage::disk('gallery')->exists($filename) && in_array($filename_url, $gallery_urls_bbdd)) {
                unset($gallery_urls_bbdd[array_search($filename_url, $gallery_urls_bbdd)]);

                FilesUtils::deleteFile($filename, 'gallery');
            }

            $user->gallery = json_encode($gallery_urls_bbdd);
            $user->save();

            return response()->json(['status' => true, 'message' => 'Image deleted successfully'], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['status' => false, 'error' => 'User not found'], 404);
        }

    }
}
