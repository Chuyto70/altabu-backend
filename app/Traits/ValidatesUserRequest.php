<?php

namespace App\Traits;

use App\Rules\MaxImages;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\File;

trait ValidatesUserRequest
{
    protected function validateUpdateUserRequest($request, $id)
    {
        $rules = [
            'username' => 'string|min:5|max:15|unique:users',
            'nationality' => 'string|max:50',
            'phone' => 'string|max:50',
            'city' => 'string|max:50',
            'weight' => 'numeric|max:150',
            'cup_size' => 'string|max:4',
            'hair_color' => 'string|max:255',
            'half' => 'numeric|max:1000',
            'hour' => 'numeric|max:2000',
            'night' => 'numeric|max:5000',
            '69_position' => 'boolean',
            'blowjob_without_condom' => 'boolean',
            'girlfrind_Experience' => 'boolean',
            'erotic_massage' => 'boolean',
            'tantric_massage' => 'boolean',
            'foam_massage' => 'boolean',
            'deepthroat' => 'boolean',
            'dirty_talk' => 'boolean',
            'facesitting' => 'boolean',
            'gangbang' => 'boolean',
            'for_couples' => 'boolean',
            'BDSM' => 'boolean',
            'bondage' => 'boolean',
            'fetish' => 'boolean',
            'foot_Fetish' => 'boolean',
            'leather' => 'boolean',
            'mistress' => 'string|max:255',
            'facebook' => 'string|max:255',
            'instagram' => 'string|max:255',
            'twitter' => 'string|max:255',
            'onlyfans' => 'string|max:255',
            'pornhub' => 'string|max:255',
            'avatar' => [
                File::image()
                    ->min('1kb')
                    ->max('5mb'),
            ],
            'description' => 'string|max:510',
            'gallery' => ['array', 'min:1', new MaxImages($id)],
            'gallery.*' => 'file|mimes:jpg,png,jpeg,webp|max:5120',
            // VIdeo max sized 10mb
            'video' => ['max:10485760',
                File::types(['mp4', 'mov', 'avi', 'wmv'])],
            'language' => ['string', Rule::in(Config::get('dashboard.locales'))],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            throw new HttpResponseException(response()->json(['status' => false, 'errors' => $validator->errors()], 422));
        }
    }

    protected function validateStoreUserRequest($request)
    {
        $rules = [
            'username' => 'required|string|max:15|unique:users',
            'nationality' => 'required|string|max:50',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|confirmed|min:6|max:16',
            'phone' => 'required|string|max:50',
            'whatsapp' => 'required|string|max:50',
            'city' => 'required|string|max:50',
            'gender' => ['required', Rule::in(['male', 'female', 'other'])],
            'age' => 'required|numeric|max:125',
            'height' => 'required|numeric|max:250',
            'weight' => 'required|numeric|max:150',
            'cup_size' => 'required|string|max:4',
            'hair_color' => 'required|string|max:255',
            'etnicity' => ['required', 'string', 'max:255', Rule::in(['hispanic', 'european', 'russian', 'asian', 'arabic', 'african', 'american', 'oceanic'])],
            'half' => 'numeric|max:1000',
            'hour' => 'numeric|max:2000',
            'night' => 'numeric|max:5000',
            '69_position' => 'boolean',
            'blowjob_without_condom' => 'boolean',
            'girlfrind_Experience' => 'boolean',
            'erotic_massage' => 'boolean',
            'tantric_massage' => 'boolean',
            'foam_massage' => 'boolean',
            'deepthroat' => 'boolean',
            'dirty_talk' => 'boolean',
            'facesitting' => 'boolean',
            'gangbang' => 'boolean',
            'for_couples' => 'boolean',
            'BDSM' => 'boolean',
            'bondage' => 'boolean',
            'fetish' => 'boolean',
            'foot_Fetish' => 'boolean',
            'leather' => 'boolean',
            'mistress' => 'boolean',
            'facebook' => 'string|max:255',
            'instagram' => 'string|max:255',
            'twitter' => 'string|max:255',
            'onlyfans' => 'string|max:255',
            'pornhub' => 'string|max:255',
            'avatar' => ['required',
                File::image()
                    ->min('1kb')
                    ->max('5mb'),
            ],
            'description' => 'string|max:510',
            'gallery' => 'required|array|min:1|max:6',
            'gallery.*' => 'required|file|mimes:jpg,png,jpeg,webp|max:5120',
            // VIdeo max sized 10mb
            'video' => ['max:10485760',
                File::types(['mp4', 'mov', 'avi', 'wmv'])],
            'language' => ['string', 'required', Rule::in(Config::get('dashboard.locales'))],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            throw new HttpResponseException(response()->json(['status' => false, 'errors' => $validator->errors()], 422));
        }
    }
}
