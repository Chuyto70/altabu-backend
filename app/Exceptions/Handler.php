<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        $rendered = parent::render($request, $exception);
        $content = json_decode($rendered->getContent());

        switch (true) {
            case $exception instanceof AuthenticationException:
                return response()->json([
                    'result' => false,
                    'type' => 'error',
                    'data' => [
                        'message' => 'translations.auth.unauthenticated',
                    ],
                ], 401);
                break;
            case $exception instanceof NotFoundHttpException:
                return response()->json([
                    'result' => false,
                    'type' => 'error',
                    'data' => [
                        'message' => 'common.error.not_found',
                    ],
                ], 401);
                break;

        }

        if (isset($content->errors)) {
            if (is_array($content->errors)) {
                $errors = $content->errors[0];
            } else {
                $errors = (array) $content->errors;
            }
        } else {
            $errors = $content->message;
        }

        return response()->json([
            'result' => false,
            'type' => 'error',
            'data' => [
                'code' => $rendered->getStatusCode(),
                'message' => $content->message,
                'errors' => $errors,
            ],
        ], $rendered->getStatusCode());
    }
}
