<?php

namespace App\Repositories\PaymentService;

use GuzzleHttp\Client;

class PaymentServiceRepository
{
    protected $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => env('PAYMENT_SERVICE_MANAGER_BASE_URL'),
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    public function refund(array $refundData)
    {
        $response = $this->httpClient->post('/api/refund/'.$refundData['transaction_id'], [
            'json' => $refundData,
        ]);

        return $response->getBody()->getContents();
    }

    public function cancelSubscription(array $subscriptionData)
    {
        $response = $this->httpClient->post('/api/subscription/cancel/'.$subscriptionData['subscription_id'], [
            'json' => $subscriptionData,
        ]);

        return $response->getBody()->getContents();
    }
}
