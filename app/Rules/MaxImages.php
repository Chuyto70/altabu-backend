<?php

namespace App\Rules;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class MaxImages implements ValidationRule
{
    public $id;
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $user = User::find($this->id);
       
       
        // Contar el número de imágenes que el usuario tiene en la base de datos
        $arr_gallery = json_decode($user->gallery);
        $currentImages = count($arr_gallery);
       
        // Contar el número de imágenes que el usuario quiere subir en el request
        $newImages = count($value);
        // Sumar los dos números
        $totalImages = $currentImages + $newImages;
        
        // Comparar el número total con el límite
        $limit = 5;
        if($totalImages > $limit){
            $fail('Limit of images exceed');
        }
    }
}
