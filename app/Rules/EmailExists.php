<?php

namespace App\Rules;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Validation\ValidationRule;

class EmailExists implements ValidationRule
{
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $exists = DB::connection($this->connection)
                 ->table('customers')
                 ->where('email', $value)
                 ->exists();
                 
        if (!$exists) {
            $fail('common.error.customer_not_found');
        }
    }
}
