<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('username', 25)->unique();
            $table->string('password', 128);
            $table->string('token')->nullable();
            $table->boolean('suspended')->default(0);
            $table->timestamp('date_suspended')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('likes')->default(0);
            $table->string('phone', 50)->nullable();
            $table->string('whatsapp', 50)->nullable();
            $table->string('city', 50);
            $table->enum('gender', ['male', 'female', 'other']);
            $table->tinyInteger('age');
            $table->smallInteger('height');
            $table->smallInteger('weight');
            $table->string('cup_size', 4);
            $table->string('hair_color', 255);
            $table->enum('etnicity', ['hispanic', 'european', 'russian', 'asian', 'arabic', 'african', 'american', 'oceanic']);
            $table->enum('nationality', ['venezuela', 'colombia', 'russia', 'china', 'alemania', 'japon', 'eeuu', 'tailandia']);
            // PRICES
            $table->smallInteger('half');
            $table->smallInteger('hour');
            $table->smallInteger('night');
            // SERVICES
            $table->boolean('69_position')->default(false);
            $table->boolean('blowjob_without_condom')->default(false);
            $table->boolean('girlfrind_Experience')->default(false);
            $table->boolean('erotic_massage')->default(false);
            $table->boolean('tantric_massage')->default(false);
            $table->boolean('foam_massage')->default(false);
            $table->boolean('deepthroat')->default(false);
            $table->boolean('dirty_talk')->default(false);
            $table->boolean('facesitting')->default(false);
            $table->boolean('gangbang')->default(false);
            $table->boolean('for_couples')->default(false);
            $table->boolean('BDSM')->default(false);
            $table->boolean('bondage')->default(false);
            $table->boolean('fetish')->default(false);
            $table->boolean('foot_Fetish')->default(false);
            $table->boolean('leather')->default(false);
            $table->boolean('mistress')->default(false);
            // ************
            // RRSS
            $table->string('facebook', 255)->nullable();
            $table->string('instagram', 255)->nullable();
            $table->string('twitter', 255)->nullable();
            $table->string('onlyfans', 255)->nullable();
            $table->string('pornhub', 255)->nullable();
            // ************
            $table->string('avatar', 255)->nullable();
            $table->string('description', 510)->nullable();
            $table->json('gallery')->nullable();
            $table->json('galleryAlts')->nullable();
            $table->string('video')->nullable();
            $table->string('videoAlt')->nullable();
            $table->string('avatarAlt')->nullable();
            $table->string('email')->unique();
            $table->string('language', 2)->nullable()->default('en');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
