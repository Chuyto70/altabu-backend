<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $validLanguages = Config::get('dashboard.locales');
        $keywords = [
            'companion',
            'massage',
            'adult',
            'erotic',
            'sensual',
            'sex',
            'escort',
            'sex',
            'intimacy',
            'intimate service',
            'lover',
            'sensual experience',
            'adult entertainment',
            'pleasure',
            'intimate encounter',
        ];
        $gallery = [];
        $galleryAlts = [];
        for ($i = 0; $i < 5; $i++) {
            $gallery[] = fake()->imageUrl();
            $galleryAlts[] = fake()->randomElements($keywords, 4);
        }
        $json = json_encode($gallery);
        $jsonGalleryAlts = json_encode($galleryAlts);

        return [
            // 'name' => fake()->name(),
            'username' => fake()->userName(),
            'email' => fake()->unique()->safeEmail(),
            'password' => Hash::make(fake()->word()),
            'phone' => fake()->phoneNumber(),
            'whatsapp' => fake()->phoneNumber(),
            'city' => fake()->city(),
            'age' => fake()->numberBetween(18, 60),
            'height' => fake()->numberBetween(150, 250),
            'weight' => fake()->numberBetween(50, 90),
            'cup_size' => fake()->numberBetween(30, 34),
            'hair_color' => fake()->colorName(),
            'etnicity' => fake()->randomElement(['hispanic', 'european', 'russian', 'asian', 'arabic', 'african', 'american', 'oceanic']),
            'nationality' => fake()->randomElement(['venezuela', 'colombia', 'russia', 'china', 'alemania', 'japon', 'eeuu', 'tailandia']),
            'half' => fake()->numberBetween(50, 100),
            'hour' => fake()->numberBetween(100, 150),
            'night' => fake()->numberBetween(200, 300),
            'gender' => fake()->randomElement(['male', 'female', 'other']),
            'avatar' => fake()->imageUrl(),
            'description' => fake()->text(40),
            'gallery' => $json,
            'galleryAlts' => $jsonGalleryAlts,
            'video' => fake()->imageUrl(),
            'videoAlt' => implode(' ', fake()->randomElements($keywords, 4)),
            'avatarAlt' => implode(' ', fake()->randomElements($keywords, 4)),
            'language' => $validLanguages[array_rand($validLanguages)],
            // services
            '69_position' => fake()->boolean(),
            'blowjob_without_condom' => fake()->boolean(),
            'girlfrind_Experience' => fake()->boolean(),
            'erotic_massage' => fake()->boolean(),
            'tantric_massage' => fake()->boolean(),
            'foam_massage' => fake()->boolean(),
            'deepthroat' => fake()->boolean(),
            'dirty_talk' => fake()->boolean(),
            'facesitting' => fake()->boolean(),
            'gangbang' => fake()->boolean(),
            'for_couples' => fake()->boolean(),
            'BDSM' => fake()->boolean(),
            'bondage' => fake()->boolean(),
            'fetish' => fake()->boolean(),
            'foot_Fetish' => fake()->boolean(),
            'leather' => fake()->boolean(),
            'mistress' => fake()->boolean(),
            // RSS
            'facebook' => fake()->imageUrl(),
            'instagram' => fake()->imageUrl(),
            'twitter' => fake()->imageUrl(),
            'onlyfans' => fake()->imageUrl(),
            'pornhub' => fake()->imageUrl(),

        ];
    }
}
