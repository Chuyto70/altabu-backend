<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Permission::create(['name' => 'manager']);
        Permission::create(['name' => 'developer']);
        Permission::create(['name' => 'support']);

        Role::create(['name' => 'super-admin']);
        
        $managerRole = Role::create(['name' => 'manager']);
        $developerRole = Role::create(['name' => 'developer']);
        $supportRole = Role::create(['name' => 'support']);

        $managerRole->givePermissionTo([
            'manager',
        ]);

        $developerRole->givePermissionTo([
            'developer',
        ]);

        $supportRole->givePermissionTo([
            'support',
        ]);
    }
}
