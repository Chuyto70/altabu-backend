<?php

namespace Database\Seeders;

use Database\Factories\UserFactory;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        UserFactory::times(200)->create();

        $this->call([
            // RoleAndPermissionSeeder::class,
            UserSeeder::class,
        ]);
    }
}
