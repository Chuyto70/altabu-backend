<?php

namespace Tests\Controllers;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;
    
    /**
     * Creates a user and authenticates the test 
     * instance as that user.
     */
    protected function authenticateUser()
    {
        $user = User::factory()->create();
        $user->assignRole('super-admin');
        $this->actingAs($user);
    }

    /**
     * Tests retrieving users
     */
    public function testIndex(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        User::factory()->count(10)->create();

        // Act
        $response = $this->getJson('/api/users');

        // Assert
        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json->where('result', true)
                ->where('type', 'success')
                ->has('data', fn (AssertableJson $json) => $json->where('current_page', 1)
                    ->where('from', 1)
                    ->hasAll([
                        'first_page_url',
                        'next_page_url',
                        'path',
                        'per_page',
                        'prev_page_url',
                        'to',
                    ])
                    ->has('data.0', fn (AssertableJson $json) => $json->hasAll([
                        'id',
                        'name',
                        'email',
                        'language',
                    ]))
                )
            );
    }

    /**
     * Tests creating a new user
     */
    public function testStore(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $data = [
            "name" => "John Doe",
            "email" => "johndoe@example.com",
            "password" => "secretPassword",
            "language" => "en",
            "role" => ["Manager"],
        ];

        // Act
        $response = $this->postJson('/api/users', $data);

        // Assert
        $response
            ->assertStatus(201)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [ 
                    'id',
                    'name',
                    'email',
                    'language',
                ],
            ]);

        $user = Arr::except($data, ['password', 'role']);

        $this->assertDatabaseHas('users', $user);
    }

    /**
     * Tests creating a new user with missing fields
     */
    public function testStoreInvalid(): void
    {
        // Auth user
        $this->authenticateUser();

        // Act
        $response = $this->postJson('/api/users');

        // Assert
        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'data' => [ 
                    'message',
                    'errors' => [
                        "name",
                        "email",
                        "password",
                    ]
                ],
            ]);
    }

    /**
     * Tests updating a user
     */
    public function testUpdate(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $user = User::factory()->create();

        $data = [
            "name" => fake()->name(),
            "email" => fake()->unique()->safeEmail(),
            "role" => ["Manager"],
        ];

        // Act
        $response = $this->patchJson('/api/users/' . $user->id, $data);

        // Assert
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [ 
                    'id',
                    'name',
                    'email',
                    'language',
                ],
            ]);

        $user = Arr::except($data, 'role');

        $this->assertDatabaseHas('users', $user);
    }

    /**
     * Tests updating an unexisting user
     */
    public function testUpdateNotFound(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $userId = 100;
        
        $data = [
            "name" => fake()->name(),
            "email" => fake()->unique()->safeEmail(),
        ];

        // Act
        $response = $this->patchJson('/api/users/' . $userId, $data);

        // Assert
        $response
            ->assertStatus(404)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'data' => [
                    'code',
                    'message',
                    'errors',
                ],
            ]);

        $this->assertDatabaseMissing('users', ['id' => $userId]);
    }

    /**
     * Tests updating a user with invalid input
     */
    public function testUpdateInvalid(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $user = User::factory()->create();
        
        $data = [
            "name" => fake()->name(),
            "email" => 100,
        ];

        // Act
        $response = $this->patchJson('/api/users/' . $user->id, $data);

        // Assert
        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'data' => [ 
                    'code',
                    'message',
                    'errors' => [
                        "email",
                    ]
                ],
            ]);
    }

    /**
     * Tests deleting a user
     */
    public function testDestroy(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $user = User::factory()->create();

        // Act
        $response = $this->deleteJson('/api/users/' . $user->id);

        // Assert
        $response->assertStatus(204);

        $this->assertDatabaseMissing('users', ['id' => $user->id]);
    }

    /**
     * Tests deleting an unexisting user
     */
    public function testDestroyNotFound(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $userId = 100;

        // Act
        $response = $this->deleteJson('/api/users/' . $userId);

        // Assert
        $response
            ->assertStatus(404)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'data' => [ 
                    'message',
                ],
            ]);

        $this->assertDatabaseMissing('users', ['id' => $userId]);
    }

}
