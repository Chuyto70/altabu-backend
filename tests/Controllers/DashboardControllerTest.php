<?php

namespace Tests\Controllers;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DashboardControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    private $token;
    
    /**
     * Creates a user and authenticates the test 
     * instance as that user.
     */
    protected function authenticateUser()
    {
        $user = User::factory()->create();
        $user->assignRole('super-admin');

        $this->token = $user->createToken(env('APP_NAME'))->plainTextToken;
        
        $this->actingAs($user);
    }

    /**
     * Tests get config
     */
    public function testGetConfigWithAuthentication()
    {
        // Auth user
        $this->authenticateUser();

        // Act
        $response = $this->getJson('/api/get-config', ['Authorization' => 'Bearer ' . $this->token]);

        // Assert
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [
                    'locales',
                    'user' => [
                        'id',
                        'name',
                        'email',
                        'language',
                        'roles' => [[
                            'id',
                            'name',
                        ]],
                    ],
                ],
            ]);
    }

}
