<?php

namespace Tests\Controllers;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * Tests user login
     */
    public function testLogin()
    {
        $knownPlainTextPassword = 'testPassword';

        $user = User::factory()->create([
            'password' => Hash::make($knownPlainTextPassword),
        ]);

        $data = [
            'email' => $user->email,
            'password' => $knownPlainTextPassword,
            'remember_me' => 0,
        ];

        $response = $this->postJson('/api/login', $data);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [
                    'token',
                ],
            ]);
    }

    public function testLoginInvalidParameters()
    {
        $invalidEmail = 'invalid@test.com';
    
        $knownPlainTextPassword = 'testPassword';

        $data = [
            'email' => $invalidEmail,
            'password' => $knownPlainTextPassword,
            'remember_me' => 0,
        ];

        $response = $this->postJson('/api/login', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])->assertJsonStructure([
                'result',
                'type',
                'data' => [
                    'message',
                ],
            ]);

    }

    public function testLogout()
    {
        $user = User::factory()->create();
        $token = $user->createToken(env('APP_NAME'))->plainTextToken;

        $response = $this->postJson('/api/logout', [], ['Authorization' => 'Bearer '.$token]);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [
                    'message',
                ],
            ]);
    }

    public function testLogoutWithInvalidToken()
    {
        $invalidToken = 'test';

        $response = $this->postJson('/api/logout', [], ['Authorization' => 'Bearer '.$invalidToken]);

        $response
            ->assertStatus(401)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [
                    'message',
                ],
            ]);
    }

    public function testRefresh()
    {
        $user = User::factory()->create();
        $token = $user->createToken(env('APP_NAME'))->plainTextToken;

        $response = $this->postJson('/api/refresh', [], ['Authorization' => 'Bearer '.$token]);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [
                    'token',
                ],
            ]);
    }

    public function testRefreshWithInvalidToken()
    {
        $invalidToken = 'test';

        $response = $this->postJson('/api/refresh', [], ['Authorization' => 'Bearer '.$invalidToken]);

        $response
            ->assertStatus(401)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [
                    'message',
                ],
            ]);
    }

    public function testGetUserData()
    {
        $user = User::factory()->create();
        $token = $user->createToken(env('APP_NAME'))->plainTextToken;

        $response = $this->getJson('/api/get-user-data', ['Authorization' => 'Bearer '.$token]);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [
                    'id',
                    'name',
                    'email',
                    'language',
                ],
            ]);
    }

    public function testGetUserDataWitInvalidToken()
    {
        $invalidToken = 'test';

        $response = $this->getJson('/api/get-user-data', ['Authorization' => 'Bearer '.$invalidToken]);

        $response
            ->assertStatus(401)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [
                    'message',
                ],
            ]);
    }

}
