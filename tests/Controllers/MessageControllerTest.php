<?php

namespace Tests\Controllers;

use Tests\TestCase;
use App\Models\User;
use App\Models\Message;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MessageControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;
    
    /**
     * Creates a user and authenticates the test 
     * instance as that user.
     */
    protected function authenticateUser()
    {
        $user = User::factory()->create();
        $user->assignRole('super-admin');
        $this->actingAs($user);
    }

    /**
     * Tests retrieving messages
     */
    public function testIndex(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        Message::factory()->count(10)->create();

        // Act
        $response = $this->getJson('/api/messages');

        // Assert
        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json->where('result', true)
                ->where('type', 'success')
                ->has('data', fn (AssertableJson $json) => $json->where('current_page', 1)
                    ->where('from', 1)
                    ->hasAll([
                        'first_page_url',
                        'next_page_url',
                        'path',
                        'per_page',
                        'prev_page_url',
                        'to',
                    ])
                    ->has('data.0', fn (AssertableJson $json) => $json->hasAll([
                        'id',
                        'user_id',
                        'customer_id',
                        'message',
                        'created_at',
                        'updated_at',
                    ]))
                )
            );
    }

    /**
     * Tests retrieving messages by given customer id
     */
    public function testShowMessagesByCustomerId(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        Message::factory()->count(10)->create();

        // Act
        $response = $this->getJson('/api/show-messages?customer_id=1');

        // Assert
        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) => $json->where('result', true)
                ->where('type', 'success')
                ->has('data', fn (AssertableJson $json) => $json->where('current_page', 1)
                    ->where('from', 1)
                    ->hasAll([
                        'first_page_url',
                        'next_page_url',
                        'path',
                        'per_page',
                        'prev_page_url',
                        'to',
                    ])
                    ->has('data.0', fn (AssertableJson $json) => $json->hasAll([
                        'id',
                        'user_id',
                        'customer_id',
                        'message',
                        'created_at',
                        'updated_at',
                    ]))
                )
            );
    }

    /**
     * Tests creating a new message
     */
    public function testStore(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $user = User::factory()->create();

        $data = [
            "user_id" => $user->id,
            "customer_id" => 1,
            "message" => "Your order has been cancelled.",
        ];

        // Act
        $response = $this->postJson('/api/messages', $data);

        // Assert
        $response
            ->assertStatus(201)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [ 
                    'id',
                    'user_id',
                    'customer_id',
                    'message',
                ],
            ]);

        $this->assertDatabaseHas('messages', $data);
    }

    /**
     * Tests creating a new message with missing fields
     */
    public function testStoreInvalid(): void
    {
        // Auth user
        $this->authenticateUser();

        // Act
        $response = $this->postJson('/api/messages');

        // Assert
        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'data' => [ 
                    'message',
                    'errors' => [
                        "user_id",
                        "customer_id",
                        "message",
                    ]
                ],
            ]);
    }

    /**
     * Tests updating a message
     */
    public function testUpdate(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $message = Message::factory()->create();

        $data = [
            "message" => "Your order has been updated.",
        ];

        // Act
        $response = $this->patchJson('/api/messages/' . $message->id, $data);

        // Assert
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'result',
                'type',
                'data' => [ 
                    'id',
                    'user_id',
                    'customer_id',
                    'message',
                    'created_at',
                    'updated_at',
                ],
            ]);

        $this->assertDatabaseHas('messages', $data);
    }

    /**
     * Tests updating an unexisting message
     */
    public function testUpdateNotFound(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $messageId = 100;
        
        $data = [
            "message" => "Your order has been updated.",
        ];

        // Act
        $response = $this->patchJson('/api/messages/' . $messageId, $data);

        // Assert
        $response
            ->assertStatus(404)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'data' => [
                    'code',
                    'message',
                    'errors',
                ],
            ]);

        $this->assertDatabaseMissing('messages', ['id' => $messageId]);
    }


    /**
     * Tests deleting a message
     */
    public function testDestroy(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $message = Message::factory()->create();

        // Act
        $response = $this->deleteJson('/api/messages/' . $message->id);

        // Assert
        $response->assertStatus(204);

        $this->assertDatabaseMissing('messages', ['id' => $message->id]);
    }

    /**
     * Tests deleting an unexisting message
     */
    public function testDestroyNotFound(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $messageId = 100;

        // Act
        $response = $this->deleteJson('/api/messages/' . $messageId);

        // Assert
        $response
            ->assertStatus(404)
            ->assertJsonFragment([
                'result' => false,
                'type' => 'error',
            ])
            ->assertJsonStructure([
                'data' => [ 
                    'message',
                ],
            ]);

        $this->assertDatabaseMissing('messages', ['id' => $messageId]);
    }

}
