<?php

namespace Tests\Controllers;

use Tests\TestCase;

class LanguageControllerTest extends TestCase
{
    public function testGetTranslations()
    {
        $baseUrl = '/api/get-translations';

        $this->json('POST', $baseUrl, [
            'language' => 'en',
        ])
        ->assertStatus(200)
        ->assertJsonFragment([
            'result' => true,
            'type' => 'success',
        ])
        ->assertJsonStructure([
            'result',
            'type',
            'data' => [
                'translations',
            ],
        ]);
    }

    public function testGetTranslationsWithInvalidParameters()
    {
        $baseUrl = '/api/get-translations';

        $this->json('POST', $baseUrl, [
            'language' => 'jp',
        ])
        ->assertStatus(422)
        ->assertJsonFragment([
            'result' => false,
            'type' => 'error',
        ])
        ->assertJsonFragment([
            'data' => [ 
                'code' => 422,
                'message' => 'common.error.unsupported_language',
                'errors' => ["common.error.unsupported_language"]
            ],
        ]);
    }

    public function testGetTranslationsWithoutParameters()
    {
        $baseUrl = '/api/get-translations';

        $this->json('POST', $baseUrl, [])
        ->assertStatus(422)
        ->assertJsonFragment([
            'result' => false,
            'type' => 'error',
        ])->assertJsonStructure([
            'data' => [ 
                'message',
                'errors' => [
                    "language"
                ]
            ],
        ]);
    }
}
