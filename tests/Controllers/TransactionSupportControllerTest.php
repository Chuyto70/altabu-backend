<?php

namespace Tests\Controllers;

use App\Models\PswMysql\Customer;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TransactionSupportControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;
    
    /**
     * Creates a user and authenticates the test 
     * instance as that user.
     */
    protected function authenticateUser()
    {
        $user = User::factory()->create();
        $user->assignRole('super-admin');
        $this->actingAs($user);
    }

    /**
     * Tests retrieving transactions
     */
    public function testSearch(): void
    {
        // Auth user
        $this->authenticateUser();

        // Act
        $response = $this->postJson('/api/transactions');

        // Assert
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'result' => true,
                'type' => 'success',
            ])
            ->assertJsonStructure([
                'data' => [
                    'data' => [[
                        'id',
                        'email',
                        'name',
                        'surname',
                        'date_of_transaction',
                        'currency',
                        'country',
                        'website',
                    ]],
                ],
            ]);
    }

    /**
     * Tests refunding a transaction
     */
    public function testRefund(): void
    {
        // Auth user
        $this->authenticateUser();

        // Arrange
        $customer = Customer::where('email',"john.nomad@test.com")->first();

        $data = [
            'transaction_id' => $customer->transactions->first()->id ?? null,
        ];

        // Act
        $response = $this->postJson('/api/refund', $data);

        // Assert
        $this->assertTrue(
            $response->status() === 200 && (
                $response->json()['result'] === true && 
                $response->json()['type'] === 'success' && 
                $response->json('data')['success'] === true && 
                isset($response->json('data')['data']['id']) && 
                $response->json('data')['data']['status'] === 'canceled'
            ) || (
                $response->json()['result'] === false && 
                $response->json()['type'] === 'error' && 
                isset($response->json('data')['code']) && 
                isset($response->json('data')['message'])
            )
        );
    }

}
