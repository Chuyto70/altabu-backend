<?php

namespace Tests\Routes;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Arr;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoleAndPermissionTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    private User $user;

    private Role $role;

    /**
     * Creates a user and authenticates the test 
     * instance as that user.
     */
    protected function authenticateUser()
    {
        $this->user = User::factory()->create();

        $this->role = Role::create(['name' => fake()->name()]);

        $this->user->assignRole($this->role);

        $this->role->givePermissionTo('manager');

        $this->actingAs($this->user);
    }

    /** @test */
    public function usersAreReturnedIfTheUserHasPermission()
    {
        // Auth user
        $this->authenticateUser();

        // Act
        $this->getJson('/api/users')->assertOk();    
    }

    /** @test */
    public function accessIsDeniedIfTheUserDoesNotHavePermission()
    {
        // Auth user
        $this->authenticateUser();

        // Revoke permission
        $this->role->revokePermissionTo('manager');

        // Act
        $this->getJson('/api/users')->assertForbidden();
    }

}
