<?php

namespace Tests\Database;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class DatabaseConnectionTest extends TestCase
{
    /**
     * Tests the Test Database Connection
     */
    public function testTestDatabaseConnection()
    {
        $databaseName = DB::connection()->getDatabaseName();

        $this->assertTrue(DB::connection()->getPdo() !== null);

        $this->assertEquals('app-test', $databaseName);
    }

    /**
     * Tests the Payment Service Manager Database Connection
     */
    public function testPdfDatabaseConnection()
    {
        // Use the 'pdf_mysql' connection to connect to the "pdf" database
        $connectionName = 'pdf_mysql';

        // Query the "psm" database using the 'pdf_mysql' connection
        $result = DB::connection($connectionName)->select('SELECT 1');

        // Assert that the query returned results
        $this->assertTrue(DB::connection($connectionName)->getPdo() !== null);

        // Assert that the query returned a result indicating a successful connection
        $this->assertEquals(1, $result[0]->{'1'});
    }

    /**
     * Tests the Payment Service Manager Database Connection
     */
    public function testPsmDatabaseConnection()
    {
        // Use the 'psm_mysql' connection to connect to the "psm" database
        $connectionName = 'psm_mysql';

        // Query the "psm" database using the 'psm_mysql' connection
        $result = DB::connection($connectionName)->select('SELECT 1');

        // Assert that the query returned results
        $this->assertTrue(DB::connection($connectionName)->getPdo() !== null);

        // Assert that the query returned a result indicating a successful connection
        $this->assertEquals(1, $result[0]->{'1'});
    }

    /**
     * Tests the Payment Service Worker Database Connection
     */
    public function testPswDatabaseConnection()
    {
        // Use the 'psw_mysql' connection to connect to the "psw" database
        $connectionName = 'psw_mysql';

        // Query the "psw" database using the 'psw_mysql' connection
        $result = DB::connection($connectionName)->select('SELECT 1');

        // Assert that the query returned results
        $this->assertTrue(DB::connection($connectionName)->getPdo() !== null);

        // Assert that the query returned a result indicating a successful connection
        $this->assertEquals(1, $result[0]->{'1'});
    }
}
