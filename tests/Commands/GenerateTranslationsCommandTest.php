<?php

namespace Tests;

use Illuminate\Support\Facades\File;

class GenerateTranslationsCommandTest extends TestCase
{
    /**
     * Login as default API user and get token back.
     *
     * @return void
     */
    public function testGenerateTranslations()
    {

        $dashboardConfigFilepath = 'config/dashboard.php';

        $this->assertFileExists($dashboardConfigFilepath, 'Dashboard config file not found');

        $locales = config("dashboard.locales");
        $this->assertGreaterThanOrEqual(1, count($locales), "no locales are defined for dashboard");

        $this->artisan('generate:translations')->assertSuccessful();

        $locales = config("dashboard.locales");

        foreach ($locales as $locale) {
            $this->assertFileExists(
                resource_path("lang/$locale/translations.json"),
                "json translations file for dashboard for language $locale not found"
            );

            $translations = json_decode(File::get(resource_path("lang/$locale/translations.json")), true);

            $directory = resource_path("lang/$locale");
            $files = array_diff(scandir($directory), ['.', '..']);

            foreach ($files as $file) {
                if (pathinfo($file)['extension'] == 'php') {
                    $filename = pathinfo($file)['filename'];
                    $this->assertArrayHasKey($filename, $translations, "translations file is missing $filename.php lines");
                }
            }

            $directory = resource_path("lang/$locale");
            $files = array_diff(scandir($directory), ['.', '..']);

            foreach ($files as $file) {
                if (array_key_exists('extension', pathinfo($file)) && pathinfo($file)['extension'] == 'php' && pathinfo($file)['filename'] !== 'mail') {
                    $filename = pathinfo($file)['filename'];
                    $this->assertArrayHasKey($filename, $translations, "translations file is missing $filename.php lines");
                }
            }

        }
    }
}
