<?php

namespace Tests;

class JsonTest extends TestCase
{
    /**
     * Try non existant route.
     *
     * @return void
     */
    public function testErrorResponse()
    {
        $baseUrl = '/api/error';

        $response = $this->call('POST', $baseUrl, [
        ]);

        $this->assertEquals(401, $response->status());
    }
}
