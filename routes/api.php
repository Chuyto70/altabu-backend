<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Middleware\SetLocaleMiddleware;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::namespace('App\Http\Controllers')
    ->middleware([SetLocaleMiddleware::class, 'check.origin'])->group(function () {

        // Admin Routes
        Route::post('admin/login', [AdminController::class, 'login'])->name('admin.login');
        Route::post('admin/register', [AdminController::class, 'register'])->name('admin.register');

        Route::middleware(['auth:sanctum', 'admin-auth'])->group(function () {
            Route::put('admin/users/suspend/{id}', 'AdminController@suspendUser');
            Route::put('admin/users/remove-suspend/{id}', 'AdminController@removeSuspendUser');
            Route::delete('admin/users/delete/{user}', 'AdminController@destroy');
            Route::post('admin/logout', 'AdminController@logout');
        });
        // User Routes
        Route::post('register', 'UserController@store');
        Route::post('login', 'AuthController@login');

        // Getting  users
        Route::get('users', 'UserController@index');
        Route::get('users/filter', 'UserController@filter');
        Route::get('users/{username}', 'UserController@getUser');

        Route::middleware(['auth:sanctum', 'verified', 'check-suspended'])->group(function () {
            Route::put('users/{id}', 'UserController@update');
            Route::post('logout', 'AuthController@logout');
            Route::delete('users/{user}/avatars/{filename}', 'UserController@deleteAvatar');
            Route::delete('users/{user}/videos/{filename}', 'UserController@deleteVideo');
            Route::delete('users/{user}/gallery/{filename}', 'UserController@deleteGallery');
        });

        // VERIFICATION MAILS ROUTES

        Route::get('/email/verify/{id}/{hash}', function ($request) {
            $user = User::find($request);

            if ($user->hasVerifiedEmail()) {
                return response()->json(['status' => true, 'message' => 'Account already verified'], 200);
            }

            if ($user->markEmailAsVerified()) {
                event(new Verified($user));

                return response()->json(['status' => true, 'message' => 'Account verified successfully'], 200);
            }

            // return redirect(env('FRONT_URL') . '/email/verify/success');
        })->middleware(['signed', 'throttle:6,1'])
            ->name('verification.verify');

        Route::post('/email/verify/resend', function (Request $request) {
            $request->user()->sendEmailVerificationNotification();

            return response()->json(['status' => true, 'message' => 'Verification code sent!'], 200);
        })->middleware(['auth:sanctum', 'throttle:6,1'])->name('verification.send');

        Route::post('/forgot-password', [ForgotPasswordController::class, 'sendResetLinkEmail'])
            ->middleware('guest')
            ->name('password.email');

        Route::get('/forgot-password/{hash}', [ForgotPasswordController::class, 'redirectToClient'])->middleware('guest')
            ->name('password.redirect');

        Route::post('/reset-password', [ForgotPasswordController::class, 'changePassword'])
            ->middleware('guest')
            ->name('password.reset');

    });
