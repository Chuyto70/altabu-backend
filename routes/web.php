<?php
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Use this for link on production

// Route::get('/linkstorage', function () {
//     if (file_exists(public_path('storage'))) {
//         return 'YA EXISTE';
//     }

//     $files = app('files');

//     try {
//         $files->link(storage_path('app/video'), public_path('video'), '-f');
//         $files->link(storage_path('app/avatars'), public_path('avatars'), '-f');
//         $files->link(storage_path('app/images_gallery'), public_path('images_gallery'), '-f');
//         return 'Has linkeado algo';
//     } catch (\Exception $e) {
//         // Registra el error o muestra un mensaje adecuado
//         return 'Error al crear los enlaces simbólicos: ' . $e->getMessage();
//     }
// });


